from django.urls import path
from . import views


urlpatterns = [
    path("register", views.register_view),
    path("login", views.login_view),
    # 地址管理[新增和查询]
    path("<str:username>/address", views.AddressView.as_view()),
    # 修改和删除
    path("<str:username>/address/<int:id>", views.AddressView.as_view()),
    # 设置默认地址
    path("<str:username>/address/default", views.DefaultAddressView.as_view()),
    # 邮件激活
    path("activation", views.active_view),

    path("sms/code", views.sms_view)
]
