"""
测试注册功能
"""
import requests


url = "http://127.0.0.1:8000/v1/users/register"

data = {
    'uname': 'zhaoliying',
    'password': '123456',
    'phone': '13603263333',
    'email': 'zhaoliying@tedu.cn',
    'verify': '123456',
}
resp = requests.post(url=url, json=data).json()
# {'code': 10103, 'error': '用户名被占用'}
print(resp)
